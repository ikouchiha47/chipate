# CHIP8

chip8 interpreter/emulator to run chip8 games. this is a stepping stone to writing the gameboy emulator.

_this is to understand at a simpler level how low level computer stuff works_

### Requirements:

- zig (0.12.0 +)
- SDL2 library

### Tests

Manual test suite refering to [chip8-test-suite](https://github.com/Timendus/chip8-test-suite), _life saver_

### Resources

- https://en.wikipedia.org/wiki/CHIP-8
- https://github.com/mattmikolay/chip-8/wiki/Mastering-CHIP‐8
- https://jackson-s.me/2019/07/13/Chip-8-Instruction-Scheduling-and-Frequency.html
